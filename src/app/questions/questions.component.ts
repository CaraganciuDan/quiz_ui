import { Component, OnInit } from '@angular/core';
import {Question} from '../services/question.model';
import {QuestionService} from '../services/question.service';

@Component({
  selector: 'app-super-secret',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  questionList: Question[];
  questionIndex = 0;
  finished: boolean;
  score = 0;
  seconds = 10;
  questionNumber = 0;
  interval: any;

  constructor(public questions: QuestionService) { }

  getQuestions() {
    this.questions.getQuestionList().subscribe(data => {
      this.questionList = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Question
      });

      this.questionNumber = 1;
      this.questionIndex = 0;
      this.finished = false;
      this.score = 0;

      this.countdown();
    })
  }

  countdown() {
    this.interval = setInterval(()=> {
      this.seconds--;

      if (this.seconds === 0) {
        this.seconds = 10;
        this.questionIndex++;
        this.questionNumber++
      }
    }, 1000)
  }

  checkQuestion(questionId: string, response: string) {
    this.questions.checkQuestion(questionId, response).subscribe( res => {
      const { isCorrect } = res.data;

      if (isCorrect) {
        this.score++;
      }

      this.seconds = 10;

      this.questionIndex++;

      this.questionNumber++;

      if (!this.questionList[this.questionIndex] || this.questionNumber > 5) {
        this.questionNumber = 0;
        clearInterval(this.interval);
        this.finished = true;
      }
    })
  }

  ngOnInit() {
  }

}
