import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import configPlaceholder from '../env';
import {QuestionsComponent} from './questions/questions.component';
import {AuthTokenHttpInterceptorProvider} from './http-interceptors/auth-token.interceptor';

@NgModule({
  declarations: [AppComponent, QuestionsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(configPlaceholder),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    HttpClientModule
  ],
  providers: [AuthTokenHttpInterceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule {}
