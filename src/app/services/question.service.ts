import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import {
  AngularFirestore,
} from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class QuestionService {

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private http: HttpClient
  ) {
  }

  public getQuestionList(): Observable<any> {
    return this.afs.collection('questions').stateChanges();
  };

  public checkQuestion(questionId: string, response: string): Observable<any> {
    return this.http.post('api/questions', {questionId, response});
  }

}

