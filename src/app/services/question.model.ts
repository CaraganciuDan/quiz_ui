export interface Question {
  id: string;
  question: string;
  responseList: string[];
  correctResponse: string;
  payload?: any;
}
